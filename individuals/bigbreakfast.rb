require 'nokogiri'
require 'open-uri'
require 'csv'

@count = 0


  def init
    puts "Name of city?  "
    print "City: "
    @city = gets.chomp

    puts "Insert Airbnb url"
    print "link: "
    @url = gets.chomp
  end

  def page_one
    data = Nokogiri::HTML(open(@url))
    links = data.css('a')
    hrefs = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}

    pagination = data.css('.pagination')
    page_array = pagination.css('a').to_a
    @last_page = page_array[3].attribute('target').to_s.to_i
    @next_page = 'https://www.airbnb.ca' + page_array[4].attribute('href').to_s

    @users = []


    hrefs.each { |link|
      if ( link =~ /show(.*)/ )
        url = 'https://www.airbnb.ca' + link
        @users << url
      end
    }
  end

  def collect_users
    @last_page.times do |i|
      data = Nokogiri::HTML(open(@next_page))
      links = data.css('a')
      hrefs = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}

      hrefs.each { |link|
        if ( link =~ /show(.*)/ )
          @url = 'https://www.airbnb.ca' + link
          @users.push(@url) unless @users.include?(@url)
          puts @users
        end
      }

      print @users.length.to_s + " Users Collected" + "\r"

      pagination = data.css('.pagination')
      page_array = pagination.css('a').to_a
      page_array_last = page_array.length
      @next_page = 'https://www.airbnb.ca' + page_array[page_array_last - 1].attribute('href').to_s
      page_array = []
    end
  end

  def collect_listings
    @name = []
    @listings = []

    @users.each { |hosts|
      userdata = Nokogiri::HTML(open(hosts))
      name = userdata.css('title').to_s.sub('<title>', '').sub("\'s Profile - Airbnb</title>", "")
      @name << name
      block = userdata.css('.listings')
      listings = block.css('small').to_s.sub('<small>(', '').sub(")</small>", "")
      @listings << listings
      print @listings.length.to_s + " Users Anaylized" + "\r"
    }
  end

  def compile_spreadsheet
    table = [@users, @name, @listings].transpose
    CSV.open("#{@city}.csv", "wb") do |csv|
      table.each do |row|
        csv << row
      end
    end
  end




  init
  page_one
  puts ""
  puts "Collecting users"
  puts ""
  collect_users
  puts "#{@users.length} Users Collected"
  puts ""
  collect_listings
  compile_spreadsheet
  puts "Generating Spreadsheet..."
  puts ""
  puts @city + ".csv created"




