require 'nokogiri'
require 'open-uri'
require 'csv'




  def init
    puts "Name of city?  "
    print "City: "
    @city = gets.chomp

    puts "Insert Airbnb url"
    print "link: "
    @url = gets.chomp
  end

  def page_one
    data = Nokogiri::HTML(open(@url))
    links = data.css('a')
    hrefs = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}

    pagination = data.css('.pagination')
    page_array = pagination.css('a').to_a
    @last_page = page_array[3].attribute('target').to_s.to_i
    @next_page = 'https://www.airbnb.com' + page_array[4].attribute('href').to_s

    @users = []


    hrefs.each { |link|
      if ( link =~ /show(.*)/ )
        url = 'https://www.airbnb.com' + link
        @users << url
      end
    }
  end

  def collect_users
    @last_page.times do |i|
      data = Nokogiri::HTML(open(@next_page))
      links = data.css('a')
      hrefs = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}

      hrefs.each { |link|
        if ( link =~ /show(.*)/ )
          @url = 'https://www.airbnb.com' + link
          @users.push(@url) unless @users.include?(@url)
          puts @users
        end
      }

      print @users.length.to_s + " Users Collected" + "\r"

      pagination = data.css('.pagination')
      page_array = pagination.css('a').to_a
      page_array_last = page_array.length
      @next_page = 'https://www.airbnb.com' + page_array[page_array_last - 1].attribute('href').to_s
      page_array = []
    end
  end

  def collect_listings
    @name = []
    @listings = []
    @guestroom = []

    @users.each { |hosts|
      userdata = Nokogiri::HTML(open(hosts))
 	  reviews = userdata.css('.reviews')
	  para = reviews.css('p')
	  para_array = para.map {|para| para}
	  para_check = para_array.to_s.downcase
      if para_check.include? "guesty"
      	puts "String includes 'Guesty'"
   		@guestroom.push(hosts)
      end
     para_array = []
    }
  end



  init
  page_one
  puts ""
  puts "Collecting users"
  puts ""
  collect_users
  puts "#{@users.length} Users Collected"
  puts ""
  collect_listings
  puts @guestroom.to_s
