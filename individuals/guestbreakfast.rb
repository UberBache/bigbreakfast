require 'nokogiri'
require 'open-uri'
require 'csv'


  def init
    puts "Name of city?  "
    print "City: "
    @city1 = gets.chomp

    puts "Insert Airbnb url"
    print "link: "
    @url1 = gets.chomp
  end

  def page_one
    data = Nokogiri::HTML(open(@url1))
    links = data.css('a')
    hrefs = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}

    pagination = data.css('.pagination')
    page_array = pagination.css('a').to_a
    @last_page1 = page_array[3].attribute('target').to_s.to_i
    @next_page1 = 'https://www.airbnb.com' + page_array[4].attribute('href').to_s

    @users1 = []


    hrefs.each { |link|
      if ( link =~ /show(.*)/ )
      	@id = link.sub('/users/show/', '')
        url = 'https://www.airbnb.com/s?host_id=' + @id
        @users1 << url
      end
    }
  end

puts "Done Page One"

    def collect_users
    @last_page1.times do |i|
      data = Nokogiri::HTML(open(@next_page1))
      links = data.css('a')
      hrefs = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}

      hrefs.each { |link|
        if ( link =~ /show(.*)/ )
          @id = link.sub('/users/show/', '')
          @url1 = 'https://www.airbnb.com' + 'https://www.airbnb.com/s?host_id=' + @id
          @users1.push(@url1) unless @users1.include?(@url1)
        end
      }

      print @users1.length.to_s + " Users Collected" + "\r"

      pagination = data.css('.pagination')
      page_array = pagination.css('a').to_a
      page_array_last = page_array.length
      @next_page1 = 'https://www.airbnb.com' + page_array[page_array_last - 1].attribute('href').to_s
      page_array = []
    end
  end




init
page_one
collect_users


puts "Done Collecting users"


@users1.each {|hosts|

data = Nokogiri::HTML(open(hosts))
@rooms = []

links = data.css('a')
hrefs = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}

hrefs.each { |link|
if ( link =~ /rooms\/(.*)/ )
  @url = 'https://www.airbnb.com' + link
  @rooms.push(@url) unless @rooms.include?(@url)
  @rooms.each {|link| 
  	if (link == "https://www.airbnb.com/rooms/new")
  		@rooms.delete(link)
  	end
  }
end
}

puts "Done Collecting Rooms"

@guestys = []
@guestroom = []
@chez = 0

@rooms.each { |link|
	puts link.to_s + " Room being scanned"
	data = Nokogiri::HTML(open(link))
	reviews = data.css('.review-content')
	para = reviews.css('p')
	para_array = para.map {|para| para}
	para_check = para_array.to_s.downcase
if para_check.include? "guesty"
   puts "String includes 'Guesty'"
   @guestroom.push(link)
end
      pagination = data.css('.pagination')
      page_array = pagination.css('a').to_a
      last_page_length = page_array.length + 1
      @last_page = last_page_length.to_i
      stopchar = "?"
      review_link = link.slice(0..(link.index('?')))
      @next_page = review_link + "reviews_page=2"
      page_array = []


@count = 2

@last_page.times do |i|
	@count = @count + 1
	puts @next_page.to_s + " review page being scanned"
	data = Nokogiri::HTML(open(@next_page))
	reviews = data.css('.review-content')
	para = reviews.css('p')
	para_array = para.map {|para| para}
	para_check = para_array.to_s.downcase
if para_check.include? "guesty"
   puts "String includes 'Guesty'"
   @guestroom.push(link)
end
      pagination = data.css('.pagination')
      page_array = pagination.css('a').to_a
      @last_page = page_array.length + 1
      stopchar = "?"
      review_link = link.slice(0..(link.index('?')))
      @next_page = review_link + "reviews_page=" + @count.to_s
      page_array = []
end
}
}

puts @guestroom

@guestroom.each {|guest| 
	data = Nokogiri::HTML(open(guest))
	href = data.css('a')
	href = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}
	href.each { |link|
      if ( link =~ /show(.*)/ )
        url = 'https://www.airbnb.com' + link
        @guestys << url unless @guestys.include?(url)
      end
    }
   }


puts "Guestys..."
puts @guestys





