require 'nokogiri'
require 'open-uri'
require 'csv'

puts ""
puts "Welcome to Bache's Air Diner Hun'"
puts "...Wanna see the menu?"
puts ""
puts "[1] Collect all listings in a city and compile them into a spreadsheet"
puts "[2] Scan a city and find the average, mean and mode prices for a market"
puts "[3] Scrape all of airbnb, compile into a spreadsheet"
puts ""
print "Number:"
answer = gets.chomp

if answer === "1" 
@count = 0


  def init
    puts "Name of city?  "
    print "City: "
    @city = gets.chomp

    puts "Insert Airbnb url"
    print "link: "
    @url = gets.chomp
  end

  def page_one
    data = Nokogiri::HTML(open(@url))
    links = data.css('a')
    hrefs = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}

    pagination = data.css('.pagination')
    page_array = pagination.css('a').to_a
    @last_page = page_array[3].attribute('target').to_s.to_i
    @next_page = 'https://www.airbnb.com' + page_array[4].attribute('href').to_s

    @users = []


    hrefs.each { |link|
      if ( link =~ /show(.*)/ )
        url = 'https://www.airbnb.com' + link
        @users << url
      end
    }
  end

  def collect_users
    @last_page.times do |i|
      data = Nokogiri::HTML(open(@next_page))
      links = data.css('a')
      hrefs = links.map {|link| link.attribute('href').to_s}.uniq.sort.delete_if {|href| href.empty?}

      hrefs.each { |link|
        if ( link =~ /show(.*)/ )
          @url = 'https://www.airbnb.com' + link
          @users.push(@url) unless @users.include?(@url)
          puts @users
        end
      }

      print @users.length.to_s + " Users Collected" + "\r"

      pagination = data.css('.pagination')
      page_array = pagination.css('a').to_a
      page_array_last = page_array.length
      @next_page = 'https://www.airbnb.com' + page_array[page_array_last - 1].attribute('href').to_s
      page_array = []
    end
  end

  def collect_listings
    @name = []
    @listings = []

    @users.each { |hosts|
      userdata = Nokogiri::HTML(open(hosts))
      name = userdata.css('title').to_s.sub('<title>', '').sub("\'s Profile - Airbnb</title>", "")
      @name << name
      block = userdata.css('.listings')
      listings = block.css('small').to_s.sub('<small>(', '').sub(")</small>", "")
      @listings << listings
      print @listings.length.to_s + " Users Anaylized" + "\r"
    }
  end

  def compile_spreadsheet
    table = [@users, @name, @listings].transpose
    CSV.open("#{@city}.csv", "wb") do |csv|
      table.each do |row|
        csv << row
      end
    end
  end




  init
  page_one
  puts ""
  puts "Collecting users"
  puts ""
  collect_users
  puts "#{@users.length} Users Collected"
  puts ""
  collect_listings
  compile_spreadsheet
  puts "Generating Spreadsheet..."
  puts ""
  puts @city + ".csv created"





elsif answer === "2" 

@count = 0


  def init
    puts "Name of city?  "
    print "City: "
    @city = gets.chomp

    puts "Insert Airbnb url"
    print "link: "
    @url = gets.chomp
  end

  def page_one
    data = Nokogiri::HTML(open(@url))
    prices = data.css('.price-amount')
    price_array = prices.map {|link| link }

    pagination = data.css('.pagination')
    page_array = pagination.css('a').to_a
    @last_page = page_array[3].attribute('target').to_s.to_i
    @next_page = 'https://www.airbnb.com' + page_array[4].attribute('href').to_s

    @users = []

    price_array.each { |link|
        fred = link.content.to_i
        @users << fred
    }
    @sum = 0
    @average = 0
    @users.each  {|fuck|
      @sum+=fuck
      @average = @sum / @users.length
    }
  end

  def collect_users
    @last_page.times do |i|
      data = Nokogiri::HTML(open(@next_page))
      prices = data.css('.price-amount')
      price_array = prices.map {|link| link }

      price_array.each { |link|
        fred = link.content.to_i
        @users << fred
      }
      @sums = 0
      @averages = 0
      @users.each  {|fuck|
        @sums+=fuck
        @averages = @sums / @users.length
      }


      pagination = data.css('.pagination')
      page_array = pagination.css('a').to_a
      page_array_last = page_array.length
      @next_page = 'https://www.airbnb.com' + page_array[page_array_last - 1].attribute('href').to_s
      page_array = []
    end

      puts @users.length.to_s + " properties"
      puts @sums.to_s + " total cost"
      puts ""
      puts "----------------------------------"
      puts @averages.to_s + " average price"
  end


def median(array)
  sorted = array.sort
  len = sorted.length
  med = (sorted[(len - 1) / 2] + sorted[len / 2]) / 2.0
  puts med.to_s + " median price"
end

def mode(x)
  sorted = x.sort
  a = Array.new
  b = Array.new
  sorted.each do |x|
    if a.index(x)==nil
      a << x # Add to list of values
      b << 1 # Add to list of frequencies
    else
      b[a.index(x)] += 1 # Increment existing counter
    end
  end
  maxval = b.max           # Find highest count
  where = b.index(maxval)  # Find index of highest count
  a[where]                 # Find corresponding data value
end

def range(array)
  array.sort!
  puts "Range..."
  puts array.first.to_s + " Least Expensive Property"
  puts array.last.to_s + " Most Expensive Property"
end




  init
  page_one
  collect_users
  median(@users)
  puts mode(@users).to_s + " mode price"
  range(@users)








elsif answer === "3" 



@count = 1
@next_page = "https://www.airbnb.com/users/show/1"
puts @next_page

  def collect_users
    10000.times do |i|
      data = Nokogiri::HTML(open(@next_page))
        @name = []
        @listings = []


        block = data.css('.listings')
        listings = block.css('small').to_s.sub('<small>(', '').sub(")</small>", "").to_i
        if listings >= 1
          @listings << listings
          name = data.css('title').to_s.sub('<title>', '').sub("\'s Profile - Airbnb</title>", "")
          @name << name
          puts @next_page
        end
        puts @name
        puts @listings
        @count = @count + 1

      @next_page = 'https://www.airbnb.com/users/show/' + @count.to_s
    end
  end


  collect_users






end
	
	
